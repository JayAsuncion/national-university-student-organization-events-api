<?php
namespace Home\Model;

class HomeCarouselItem {
    public $label;
    public $url;

    public function exchangeArray($data)
    {
        $this->label = !empty($data['label']) ? $data['label'] : '';
        $this->url = !empty($data['url']) ? $data['url'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
