<?php
namespace Home\Model;

use Zend\Db\TableGateway\TableGateway;

class HomeCarouselItemTable {
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function loadHomeCarouselItemList()
    {
        return $this->tableGateway->select(['delete_flag' => 'n']);
    }
}
