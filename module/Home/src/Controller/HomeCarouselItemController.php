<?php
namespace Home\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Home\Model\HomeCarouselItemTable;
use Zend\View\Model\JsonModel;

class HomeCarouselItemController extends AppAbstractRestfulController
{
    protected $homeCarouselItemTable;

    public function __construct(
        AuthService $authService,
        HomeCarouselItemTable $homeCarouselItemTable
    ) {
        parent::__construct($authService);
        $this->homeCarouselItemTable = $homeCarouselItemTable;
    }

    public function getList()
    {
        $homeCarouselItemListResultSet = $this->homeCarouselItemTable->loadHomeCarouselItemList();
        $homeCarouselItemList = iterator_to_array($homeCarouselItemListResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'home_carousel_item_list' => $homeCarouselItemList
            ]
        ]);
    }
}
