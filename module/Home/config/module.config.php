<?php
namespace Home;

use Home\Controller\HomeCarouselItemController;
use Home\Model\HomeCarouselItemTable;
use Home\ServiceFactory\Controller\HomeCarouselItemControllerFactory;
use Home\ServiceFactory\Model\HomeCarouselItemTableFactory;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'home-carousel-items' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/home-carousel-items[/:id]',
                    'defaults' => [
                        'controller' => HomeCarouselItemController::class
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            HomeCarouselItemController::class => HomeCarouselItemControllerFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            HomeCarouselItemTable::class => HomeCarouselItemTableFactory::class
        ],
        'invokables' => [

        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
];
