<?php
namespace Event\Model;

use Zend\Db\TableGateway\TableGateway;

class EventCategoryTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAllCategories()
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(['event_category_id', 'event_category_name']);
        $select->where(['delete_flag' => 'n']);
        return $this->tableGateway->selectWith($select);
    }
}
