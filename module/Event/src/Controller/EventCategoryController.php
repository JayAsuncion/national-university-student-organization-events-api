<?php
namespace Event\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Event\Model\EventCategoryTable;
use Zend\View\Model\JsonModel;

class EventCategoryController extends AppAbstractRestfulController
{
    private $categoryTable;

    public function __construct(
        AuthService $authService,
        EventCategoryTable $categoryTable
    ) {
        parent::__construct($authService);
        $this->categoryTable = $categoryTable;
    }

    public function getList()
    {
        $categoriesResultSet = $this->categoryTable->fetchAllCategories();
        $categories = iterator_to_array($categoriesResultSet->getDataSource());
        $response = [
            'success' => true,
            'data' => [
                'event_category_list' => $categories
            ]
        ];
        return new JsonModel($response);
    }
}
