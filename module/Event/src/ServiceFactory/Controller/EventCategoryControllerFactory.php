<?php
namespace Event\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Event\Controller\EventCategoryController;
use Event\Model\EventCategoryTable;
use Psr\Container\ContainerInterface;

class EventCategoryControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $categoryTable = $container->get(EventCategoryTable::class);

        return new EventCategoryController(
            $authService,
            $categoryTable
        );
    }
}
