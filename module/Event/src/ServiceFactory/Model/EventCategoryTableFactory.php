<?php
namespace Event\ServiceFactory\Model;

use Event\Model\EventCategory;
use Event\Model\EventCategoryTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class EventCategoryTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new EventCategory());

        $tableGateway = new TableGateway(
            'event_categories',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new EventCategoryTable($tableGateway);
    }
}
