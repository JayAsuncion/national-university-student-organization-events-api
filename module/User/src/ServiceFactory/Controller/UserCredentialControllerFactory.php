<?php


namespace User\ServiceFactory\Controller;


use Auth\Service\AuthService;
use Psr\Container\ContainerInterface;
use User\UserController;

class UserCredentialControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);

        return new UserController(
            $authService
        );
    }
}
