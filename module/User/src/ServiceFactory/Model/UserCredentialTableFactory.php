<?php
namespace User\ServiceFactory\Model;

use Psr\Container\ContainerInterface;
use User\Model\UserCredential;
use User\Model\UserCredentialTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class UserCredentialTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new UserCredential());

        $tableGateway = new TableGateway(
            'users_credentials',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new UserCredentialTable($tableGateway);
    }
}
