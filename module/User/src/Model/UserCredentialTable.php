<?php
namespace User\Model;


use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class UserCredentialTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchUserCredentialsByEmailAndPassword($email, $password)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(['user_credential_id', 'email', 'user_type_id', 'user_info_id', 'active_flag',
            'user_type_label' => new Expression('ut.user_type_label')]);
        $select->where(['email' => $email, 'password' => $password]);
        $select->join(['ut' => 'user_types'], 'users_credentials.user_type_id = ut.user_type_id', [], 'INNER');

        return $this->tableGateway->selectWith($select);
    }
}
