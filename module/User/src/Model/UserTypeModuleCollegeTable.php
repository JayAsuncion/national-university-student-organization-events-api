<?php
namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

class UserTypeModuleCollegeTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchUserTypeModuleCollegesUsingModuleCode($moduleCode) {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(['user_type_module_college_id', 'college_id']);
        $select->where(['module_code' => $moduleCode]);

        return $this->tableGateway->selectWith($select);
    }
}
