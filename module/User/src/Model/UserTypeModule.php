<?php
namespace User\Model;

class UserTypeModule
{
    public $user_type_module_id;
    public $user_type_id;
    public $module_code;

    public function exchangeArray($data)
    {
        $this->user_type_module_id = (!empty($data['user_type_module_id'])) ? $data['user_type_module_id'] : '';
        $this->user_type_id = (!empty($data['user_type_id'])) ? $data['user_type_id'] : '';
        $this->module_code = (!empty($data['module_code'])) ? $data['module_code'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
