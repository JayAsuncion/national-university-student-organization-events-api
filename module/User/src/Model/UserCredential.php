<?php
namespace User\Model;

class UserCredential
{
    public $user_credential_id;
    public $email;
    public $user_type_id;
    public $user_info_id;
    public $active_flag;

    public function exchangeArray($data)
    {
        $this->user_credential_id = (!empty($data['user_credential_id'])) ? $data['user_credential_id'] : 0;
        $this->email = (!empty($data['email'])) ? $data['email'] : '';
        $this->user_type_id = (!empty($data['user_type_id'])) ? $data['user_type_id'] : 0;
        $this->user_info_id = (!empty($data['user_info_id'])) ? $data['user_info_id'] : 0;
        $this->active_flag = (!empty($data['active_flag'])) ? $data['active_flag'] : 'y';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
