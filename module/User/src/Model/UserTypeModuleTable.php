<?php
namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

class UserTypeModuleTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchUserTypeModulesUsingUserTypeID($userTypeID) {
        return $this->tableGateway->select(['user_type_id' => $userTypeID]);
    }
}
