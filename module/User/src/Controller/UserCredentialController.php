<?php
namespace User\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;

class UserCredentialController extends AppAbstractRestfulController
{
    public function __construct(AuthService $authService)
    {
        parent::__construct($authService);
    }
}
