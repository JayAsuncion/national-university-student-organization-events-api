<?php
namespace User;

use User\Controller\UserCredentialController;
use User\Model\UserCredentialTable;
use User\Model\UserTypeModuleCollegeTable;
use User\Model\UserTypeModuleTable;
use User\ServiceFactory\Controller\UserCredentialControllerFactory;
use User\ServiceFactory\Model\UserCredentialTableFactory;
use User\ServiceFactory\Model\UserTypeModuleCollegeTableFactory;
use User\ServiceFactory\Model\UserTypeModuleTableFactory;
use Zend\Router\Http\Segment;

return array(
    'router' => array(
        'routes' => array (
            'user' => array(
                'type' => Segment::class,
                'options' => array(
                    'route' =>'/user[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => UserCredentialController::class
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'factories' => array(
            UserCredentialController::class => UserCredentialControllerFactory::class
        )
    ),
    'service_manager' => array(
        'factories' => array(
            UserCredentialTable::class => UserCredentialTableFactory::class,
            UserTypeModuleTable::class => UserTypeModuleTableFactory::class,
            UserTypeModuleCollegeTable::class => UserTypeModuleCollegeTableFactory::class
        ),
        'invokables' => array(

        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);
