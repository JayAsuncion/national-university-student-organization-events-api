<?php
namespace College;

use College\Controller\CollegeController;
use College\Model\CollegesTable;
use College\ServiceFactory\Controller\CollegeControllerFactory;
use College\ServiceFactory\Model\CollegesTableFactory;
use Zend\Router\Http\Segment;

return array(
    'router' => [
        'routes' => [
            'colleges' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/colleges[/:id]',
                    'defaults' => [
                        'controller' => CollegeController::class
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            CollegeController::class => CollegeControllerFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            CollegesTable::class => CollegesTableFactory::class
        ],
        'invokables' => [

        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
);
