<?php
namespace College\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use College\Model\CollegesTable;
use Zend\View\Model\JsonModel;

class CollegeController extends AppAbstractRestfulController
{
    private $collegesTable;

    public function __construct(
        AuthService $authService,
        CollegesTable $collegesTable
    ) {
        parent::__construct($authService);
        $this->collegesTable = $collegesTable;
    }

    public function getList()
    {
        $isCollege = $this->params()->fromQuery('is_college');
        $collegeListResultSet = $this->collegesTable->getCollegeList($isCollege);
        $collegeList = iterator_to_array($collegeListResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'colleges' => $collegeList
            ]
        ]);
    }

    public function get($id)
    {
        $collegeID = $id;
        $collegeResultSet = $this->collegesTable->getCollegeByCollegeID($collegeID);
        $college = $collegeResultSet->getDataSource()->current();

        if (empty($college)) {
            return new JsonModel([
                'success' => false,
                'message' => 'College #' . $id . ' not found.'
            ]);
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'college' => $college
            ]
        ]);
    }
}
