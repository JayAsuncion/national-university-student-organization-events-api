<?php
namespace College\Model;

use Zend\Db\TableGateway\TableGateway;

class CollegesTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function insertCollege($data)
    {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function updateCollege($collegeID, $data)
    {
        $update = $this->tableGateway->getSql()->update();
        $update->set($data);
        $update->where(['college_id' => $collegeID]);
        return $this->tableGateway->updateWith($update);
    }

    public function getCollegeList($isCollege = 'y')
    {
        $select = $this->tableGateway->getSql()->select();

        if ($isCollege == 'y') {
            $select->where(['is_college' => $isCollege, 'delete_flag' => 'n']);
        }

        return $this->tableGateway->selectWith($select);
    }

    public function getCollegeByCollegeID($collegeID)
    {
        return $this->tableGateway->select(['college_id' => $collegeID, 'is_college' => 'y', 'delete_flag' => 'n']);
    }
}
