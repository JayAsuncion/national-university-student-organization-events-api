<?php
namespace Auth\Service;

class AuthService
{
    private $tokenService;

    public function __construct(
        TokenService $tokenService
    ) {
        $this->tokenService = $tokenService;
    }

    public function getUserCredentialIDFromAuthHeader($authHeader)
    {
        return $this->tokenService->getValueInAccessToken($authHeader,'user_credential_id');
    }
}
