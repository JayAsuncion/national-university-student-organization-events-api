<?php
namespace Auth\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Filter\LoginFilter;
use Auth\Service\AuthService;
use Auth\Service\TokenService;
use User\Model\UserCredentialTable;
use User\Model\UserTypeModuleCollegeTable;
use User\Model\UserTypeModuleTable;
use Zend\View\Model\JsonModel;

class AuthController extends AppAbstractRestfulController
{
    private $userCredentialTable;
    private $userTypeModuleCollegeTable;
    private $userTypeModuleTable;
    private $tokenService;
    private $loginFilter;

    public function __construct(
        UserCredentialTable $userCredentialTable,
        UserTypeModuleCollegeTable $userTypeModuleCollegeTable,
        UserTypeModuleTable $userTypeModuleTable,
        AuthService $authService,
        TokenService $tokenService,
        LoginFilter $loginFilter
    ) {
        parent::__construct($authService);
        $this->userCredentialTable = $userCredentialTable;
        $this->userTypeModuleCollegeTable = $userTypeModuleCollegeTable;
        $this->userTypeModuleTable = $userTypeModuleTable;
        $this->tokenService = $tokenService;
        $this->loginFilter= $loginFilter;
    }

    /**
     * @param mixed $data (See below)
     *      $data
 *              ['email']
     *          ['password']
     * @return mixed|JsonModel|\ZF\ApiProblem\ApiProblemResponse (See below)
     *      JsonModel
     *          ['success']
     *          ['data']
     *              ['jwt_token']
     *                  [0]
     *                  [1]
     *                      ['iss']     string
     *                      ['aud']     string
     *                      ['iat']     int
     *                      ['nbf']     int
     *                      ['exp']     int
     *                      ['data']    array of array
     *                          ['user']    array of array
     *                              ['user_credential_id']      int
     *                              ['email']                   string
     *                              ['user_type_id']            int
     *                              ['user_info_id']            int
     *                              ['active_flag']             char
     *                              ['user_type_modules']       array of object
     *                                  ['user_type_module_id']         int
     *                                  ['user_type_id']                int
     *                                  ['module_code']                 string
     *                                  ['user_type_module_colleges']   array of objects
     *                                      ['user_type_module_college_id']     int
     *                                      ['college_id']                      int
     */
    public function create($data)
    {
        $this->loginFilter->setData($data);

        if (!$this->loginFilter->isValid()) {
            return $this->validationError($this->loginFilter->getMessages());
        }

        $loginFormData = $this->loginFilter->getValues();

        $userCredentialResultSet = $this->userCredentialTable->fetchUserCredentialsByEmailAndPassword(
            $loginFormData['email'],
            $loginFormData['password']
        );
        $userCredential = $userCredentialResultSet->getDataSource()->current();

        if (!$userCredential) {
            return $this->invalidCredentialsError('Invalid Credentials');
        }

        $userTypeModulesResultSet = $this->userTypeModuleTable->fetchUserTypeModulesUsingUserTypeID($userCredential['user_type_id']);
        $userTypeModules = iterator_to_array($userTypeModulesResultSet->getDataSource());
        $userCredential['user_type_modules'] = $userTypeModules;

        for ($i = 0; $i < count($userCredential['user_type_modules']); $i++) {
            $userTypeModuleCollegesResultSet = $this->userTypeModuleCollegeTable->fetchUserTypeModuleCollegesUsingModuleCode($userCredential['user_type_modules'][$i]['module_code']);
            $userTypeModuleColleges = iterator_to_array($userTypeModuleCollegesResultSet->getDataSource());
            $userCredential['user_type_modules'][$i]['user_type_module_colleges'] = $userTypeModuleColleges;
        }

        $jwtToken = $this->tokenService->generateToken([
            'user' => $userCredential
        ]);

        return new JsonModel([
            'success' => true,
            'data' => [
                'jwt_token' => $jwtToken
            ]
        ]);
    }
}
