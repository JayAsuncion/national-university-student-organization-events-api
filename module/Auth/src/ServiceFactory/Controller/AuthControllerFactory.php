<?php
namespace Auth\ServiceFactory\Controller;

use Auth\Controller\AuthController;
use Auth\Filter\LoginFilter;
use Auth\Service\AuthService;
use Auth\Service\TokenService;
use Psr\Container\ContainerInterface;
use User\Model\UserCredentialTable;
use User\Model\UserTypeModuleCollegeTable;
use User\Model\UserTypeModuleTable;

class AuthControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $userCredentialTable = $container->get(UserCredentialTable::class);
        $userTypeModuleTable = $container->get(UserTypeModuleTable::class);
        $userTypeModuleCollegeTable = $container->get(UserTypeModuleCollegeTable::class);
        $tokenService = $container->get(TokenService::class);
        $authService = $container->get(AuthService::class);
        $loginFilter = $container->get(LoginFilter::class);

        return new AuthController(
            $userCredentialTable,
            $userTypeModuleCollegeTable,
            $userTypeModuleTable,
            $authService,
            $tokenService,
            $loginFilter
        );
    }
}
