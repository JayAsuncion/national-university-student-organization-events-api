<?php
namespace Auth;

use Auth\Controller\AuthController;
use Auth\Filter\LoginFilter;
use Auth\Service\AuthService;
use Auth\Service\TokenService;
use Auth\ServiceFactory\Controller\AuthControllerFactory;
use Auth\ServiceFactory\Service\AuthServiceFactory;
use Auth\ServiceFactory\Service\TokenServiceFactory;
use Zend\Router\Http\Segment;

return array(
    'router' => array(
        'routes' => array(
            'auth' => array(
                'type' => Segment::class,
                'options' => array(
                    'route' => '/auth',
                    'defaults' => array(
                        'controller' => AuthController::class
                    )
                )
            )
        )
    ),
    'controllers' => [
        'factories' => [
            AuthController::class => AuthControllerFactory::class
        ],
    ],
    'service_manager' => [
        'factories' => [
            AuthService::class => AuthServiceFactory::class,
            TokenService::class => TokenServiceFactory::class
        ],
        'invokables' => [
            LoginFilter::class => LoginFilter::class
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
);
