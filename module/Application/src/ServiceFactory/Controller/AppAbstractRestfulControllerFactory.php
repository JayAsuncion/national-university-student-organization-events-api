<?php
namespace Application\ServiceFactory\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Psr\Container\ContainerInterface;

class AppAbstractRestfulControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        return new AppAbstractRestfulController($authService);
    }
}
