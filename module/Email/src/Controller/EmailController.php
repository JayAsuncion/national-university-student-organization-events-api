<?php
namespace Email\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use PHPMailer\PHPMailer\PHPMailer;
use Zend\View\Model\JsonModel;

class EmailController extends AppAbstractRestfulController
{
    protected $phpMailer;
    private $isDebugging = false;
//    private $SMTPCredentials = [
//        'email' => 'jintuitive.client@client.jintuitive.tech',
//        'password' => 'qse&N1JU',
//        'host' => 'smtp.hostinger.ph',
//        'port' => 587,
//        'SMTPAuth' => true
//    ];

    private $SMTPCredentials = [
        'GRIEVANCE_FORM' => [
            'email' => 'grievance-form@officialnusg.com',
            'password' => 'BOfq7ol1',
            'host' => 'smtp.hostinger.ph',
            'port' => 587,
            'SMTPAuth' => true
        ],
        'CONTACT_US_FORM' => [
            'email' => 'contact-us-form@officialnusg.com',
            'password' => 'bve?]OyN',
            'host' => 'smtp.hostinger.ph',
            'port' => 587,
            'SMTPAuth' => true
        ]
    ];

    private $gmailAccount = [
        'email' =>'nusg.website2020@gmail.com',
        'name' => 'National University Student Government'
    ];

    private $SMTPName = [
        'CONTACT_US_FORM' => 'NUSG Contact Us Form',
        'GRIEVANCE_FORM' => 'NUSG Grievance Form'
    ];

    public function __construct(
        AuthService $authService,
        PHPMailer $PHPMailer
    ) {
        parent::__construct($authService);
        $this->phpMailer = $PHPMailer;
    }

    public function create($data)
    {
        $filteredData = $data;
        $emailSourceCode = !empty($filteredData['source_form']) ? $filteredData['source_form'] : 'CONTACT_US_FORM';

        $this->phpMailer->isSMTP();
        if ($this->isDebugging) {
            $this->phpMailer->SMTPDebug = 2;
        } else {
            $this->phpMailer->SMTPDebug = 0;
        }
        $this->phpMailer->Host = $this->SMTPCredentials[$emailSourceCode]['host'];
        $this->phpMailer->Port = $this->SMTPCredentials[$emailSourceCode]['port'];
        $this->phpMailer->SMTPAuth = $this->SMTPCredentials[$emailSourceCode]['SMTPAuth'];
        $this->phpMailer->Username = $this->SMTPCredentials[$emailSourceCode]['email'];
        $this->phpMailer->Password = $this->SMTPCredentials[$emailSourceCode]['password'];
        /*
         * From email must be the same with smpt email for authentication purposes, else
         * It will return an error sender address rejected: not owned by user jintuitive.client@client.jintuitive.tech
        */
        $this->phpMailer->setFrom($this->SMTPCredentials[$emailSourceCode]['email'], $this->SMTPName[$emailSourceCode]);
        $this->phpMailer->addReplyTo($filteredData['email'], $filteredData['name']);
        $this->phpMailer->addAddress($this->gmailAccount['email'], $this->gmailAccount['name']);
        $this->phpMailer->Subject = $filteredData['subject'];

        $todayDate = date('F d, Y h:i:s A');
        $htmlContent = '
            <table>
                <tr>
                    <td><strong>DATE: </strong>' . $todayDate . '</td>                
                </tr>
                <tr>
                    <td><strong>FROM: </strong>' . $filteredData['name'] . '</td>
                </tr>
                <tr>
                    <td><strong>EMAIL: </strong>' . $filteredData['email'] . '</td>
                </tr>
                <tr>
                    <td style="width: 30%;"><strong>SUBJECT: </strong>' . $filteredData['subject'] . '</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><strong>CONCERN:</strong></td>
                </tr>
                <tr>
                    <td>' . $filteredData['concern'] . '</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><strong>SUGGESTION:</strong></td>
                </tr>
                <tr>
                    <td>' . $filteredData['suggestion'] . '</td>
                </tr>
            </table>
       ';
        $this->phpMailer->msgHTML($htmlContent);

        if ($this->phpMailer->send()) {
            return new JsonModel([
                'success' => true,
                'message' => 'Thank you for submitting. We will get back to you as soon as possible.'
            ]);
        } else {
            echo 'Error' . $this->phpMailer->ErrorInfo;
            return new JsonModel([
                'success' => false,
                'message' => 'Our grievance form is currently down. Kindly reach as at facebook instead.'
            ]);
        }
    }
}
