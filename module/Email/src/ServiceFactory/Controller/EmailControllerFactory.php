<?php
namespace Email\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Email\Controller\EmailController;
use PHPMailer\PHPMailer\PHPMailer;
use Psr\Container\ContainerInterface;

class EmailControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $phpMailer = new PHPMailer();
        return new EmailController(
            $authService,
            $phpMailer
        );
    }
}
