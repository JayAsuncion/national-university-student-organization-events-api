<?php
namespace Email;

use Email\Controller\EmailController;
use Email\ServiceFactory\Controller\EmailControllerFactory;
use Zend\Router\Http\Segment;

return array(
    'router' => [
        'routes' => [
            'emails' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/emails[/:id]',
                    'defaults' => [
                        'controller' => EmailController::class
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            EmailController::class => EmailControllerFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [

        ],
        'invokables' => [

        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
);
