<?php
namespace Page;

use Page\Controller\PageController;
use Page\Controller\PageDetailsController;
use Page\Model\PageContentsTable;
use Page\Model\PageDetailsTable;
use Page\ServiceFactory\Controller\PageControllerFactory;
use Page\ServiceFactory\Controller\PageDetailsControllerFactory;
use Page\ServiceFactory\Model\PageContentsTableFactory;
use Page\ServiceFactory\Model\PageDetailsTableFactory;
use Zend\Router\Http\Segment;

return array(
    'router' => [
        'routes' => [
            'pages' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/pages[/:id]',
                    'defaults' => [
                        'controller' => PageController::class
                    ]
                ]
            ],
            'page-details' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/page-details[/:id]',
                    'defaults' => [
                        'controller' => PageDetailsController::class
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            PageController::class => PageControllerFactory::class,
            PageDetailsController::class => PageDetailsControllerFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            PageContentsTable::class => PageContentsTableFactory::class,
            PageDetailsTable::class => PageDetailsTableFactory::class
        ],
        'invokables' => [

        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
);
