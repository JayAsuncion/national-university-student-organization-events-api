<?php
namespace Page\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Page\Model\PageContentsTable;
use Page\Model\PageDetailsTable;
use Zend\View\Model\JsonModel;

class PageDetailsController extends AppAbstractRestfulController
{
    private $pageContentsTable;
    private $pageDetailsTable;

    public function __construct(
        AuthService $authService,
        PageContentsTable $pageContentsTable,
        PageDetailsTable $pageDetailsTable
    ) {
        parent::__construct($authService);
        $this->pageContentsTable = $pageContentsTable;
        $this->pageDetailsTable = $pageDetailsTable;
    }

    public function getList()
    {
        $queryParams = $this->params()->fromQuery();
        $limitParam = (!empty($queryParams['limit'])) ? $queryParams['limit'] : '';
        $pageCategoryCode = (!empty($queryParams['page_category_code'])) ? $queryParams['page_category_code'] : 'EVENT_PAGE';

        $pageDetailsResultSet = $this->pageDetailsTable->getPagesDetails([
            'limit' => $limitParam,
            'page_category_code' => $pageCategoryCode
        ]);
        $pageDetails = iterator_to_array($pageDetailsResultSet->getDataSource());

        $index = 0;

        foreach($pageDetails as $pageDetail) {
            $pageContentResultSet = $this->pageContentsTable->fetchPageContentUsingPageDetailID($pageDetail['page_detail_id']);
            $pageContent = iterator_to_array($pageContentResultSet->getDataSource());
            $pageDetails[$index]['page_contents'] = $pageContent;
            $index ++;
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'pageList' => $pageDetails
            ]
        ]);
    }
}
