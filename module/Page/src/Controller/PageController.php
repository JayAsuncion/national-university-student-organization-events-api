<?php
namespace Page\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Page\Model\PageContentsTable;
use Page\Model\PageDetailsTable;
use Zend\View\Model\JsonModel;

class PageController extends AppAbstractRestfulController
{
    private $pageContentsTable;
    private $pageDetailsTable;

    public function __construct(
        AuthService $authService,
        PageContentsTable $pageContentsTable,
        PageDetailsTable $pageDetailsTable
    ) {
        parent::__construct($authService);
        $this->pageContentsTable = $pageContentsTable;
        $this->pageDetailsTable = $pageDetailsTable;
    }

    public function getList()
    {
        $queryParams = $this->params()->fromQuery();
        $offsetParam = (!empty($queryParams['offset'])) ? $queryParams['offset'] : '';
        $limitParam = (!empty($queryParams['limit'])) ? $queryParams['limit'] : '';
        $dateRange = array(
            'start_date' => (!empty($queryParams['start_date'])) ? $queryParams['start_date'] : '',
            'end_date' => (!empty($queryParams['end_date'])) ? $queryParams['end_date'] : ''
        );
        $collegeID = (!empty($queryParams['college_id'])) ? $queryParams['college_id'] : 1;
        $pageCategoryCode = (!empty($queryParams['page_category_code'])) ? $queryParams['page_category_code'] : 'EVENT_PAGE';
        $eventCategoryID = (!empty($queryParams['event_category_id'])) ? $queryParams['event_category_id'] : 0;

        $pageDetailsResultSet = $this->pageDetailsTable->getPagesDetailsAndContent([
                'date_range' => $dateRange,
                'college_id' => $collegeID, 'page_category_code' => $pageCategoryCode, 'event_category_id' => $eventCategoryID,
                'offset' => $offsetParam, 'limit' => $limitParam]);
        $pageDetails = iterator_to_array($pageDetailsResultSet->getDataSource());

        if (count($pageDetails) <= 0) {
            return $this->resourceNotFoundError('We could not find the event you are looking for.');
        }

        $index = 0;
        $pageDetailsCount = count($pageDetails);
        $pageList = [];
        $currentPageContentsData = [];
        $currentPageDetailsID = $pageDetails[0]['page_detail_id'];

        foreach($pageDetails as $pageDetail) {
            $isAtLastIndex = $index == ($pageDetailsCount - 1);
            array_push($currentPageContentsData, ['json_content' => $pageDetail['json_content']]);

            if ((!$isAtLastIndex && $pageDetails[$index +  1]['page_detail_id'] != $currentPageDetailsID) || $isAtLastIndex) {
                $pageListData = [
                    'page_detail_id' => $pageDetail['page_detail_id'],
                    'page_name' => $pageDetail['page_name'],
                    'page_description' => $pageDetail['page_description'],
                    'page_url' => $pageDetail['page_url'],
                    'user_credential_name' => $pageDetail['user_credential_name'],
                    'page_contents' => $currentPageContentsData
                ];
                array_push($pageList, $pageListData);
                $currentPageContentsData = [];
                if (!$isAtLastIndex) {
                    $currentPageDetailsID = $pageDetails[$index +  1]['page_detail_id'];
                }
            }
            $index ++;
        }

        $pagesCountResultSet = $this->pageDetailsTable->getPagesCount($pageCategoryCode, $eventCategoryID, $dateRange);
        $pagesCount = $pagesCountResultSet->getDataSource()->count();

        return new JsonModel([
            'success' => true,
            'data' => [
                'pagesCount' => $pagesCount,
                'pageList' => $pageList
            ]
        ]);
    }

    public function get($id)
    {
        $pageDetailsAndContentResultSet = $this->pageDetailsTable->getPageDetailsAndContent($id);
        $pageDetailsAndContent = $pageDetailsAndContentResultSet->getDataSource()->current();

        if (empty($pageDetailsAndContent))
            return new JsonModel(['success' => false, 'message' => 'Page not found.']);

        return new JsonModel([
            'success' => true,
            'data' => [
                'page_details' => $pageDetailsAndContent
            ]
        ]);
    }
}
