<?php
namespace Page\Model;

class PageContent
{
    public $page_content_id;
    public $form_template_content_id;
    public $page_detail_id;

    public function exchangeArray($data)
    {
        $this->page_content_id = (!empty($data['page_content_id'])) ? $data['page_content_id'] : 0;
        $this->form_template_content_id = (!empty($data['form_template_content_id'])) ? $data['form_template_content_id'] : 0;
        $this->page_detail_id = (!empty($data['page_detail_id'])) ? $data['page_detail_id'] : 0;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
