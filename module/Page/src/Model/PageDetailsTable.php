<?php
namespace Page\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class PageDetailsTable
{
    private $tableGateway;

    public function __construct(
        TableGateway $tableGateway
    ) {
        $this->tableGateway = $tableGateway;
    }

    public function getPagesDetails($options = array())
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'page_detail_id', 'page_name', 'page_description', 'page_url',
            'user_credential_name', 'date_created'
        ]);

        if (!empty($options['limit'])) {
            $select->limit($options['limit']);
        }

        if (!empty($options['page_category_code'])) {
            $select->where(['page_category_code' => $options['page_category_code']]);
        }

        $select->where->and->equalTo('college_id', 1);
        $select->where->and->equalTo('delete_flag', 'n');

        return $this->tableGateway->selectWith($select);
    }

    public function getPagesDetailsAndContent($options= array())
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'page_detail_id', 'page_name', 'page_description', 'page_url',
            'user_credential_name', 'date_created',
            'json_content' => new Expression('ftc.json_content'),
            'template_code' => new Expression('ftc.template_code'),
            'event_date' => new Expression('ftc.event_date')
        ]);
        $select->join(['pc' => 'page_contents'], 'page_details.page_detail_id = pc.page_detail_id', [], 'INNER');
        $select->join(['ftc' => 'form_template_contents'], 'pc.form_template_content_id = ftc.form_template_content_id', [], 'INNER');

        if (count($options)) {
            $isStartDateSet = false;
            $isEndDateSet = false;

            if (!empty($options['date_range'])) {
                if (!empty($options['date_range']['start_date'])) {
                    $select->where->greaterThanOrEqualTo('event_date', $options['date_range']['start_date']);
                    $isStartDateSet = true;
                }

                if (!empty($options['date_range']['end_date'])) {
                    if ($isStartDateSet) {
                        $select->where->and;
                    }

                    $isEndDateSet = true;
                    $select->where->lessThanOrEqualTo('event_date', $options['date_range']['end_date']);
                }
            }

            if (!empty($options['college_id'])) {
                if ($isStartDateSet || $isEndDateSet) {
                    $select->where->and;
                }

                $select->where->equalTo('page_details.college_id', $options['college_id']);
            }

            if (!empty($options['page_category_code'])) {
                $select->where->and;
                $select->where->equalTo('page_details.page_category_code', $options['page_category_code']);
            }

            if (!empty($options['event_category_id'])) {
                $select->where->and;
                $select->where->equalTo('ftc.event_category', $options['event_category_id']);
            }

            if (!empty($options['limit'])) {
                $select->limit($options['limit']);
            }

            if (!empty($options['offset'])) {
                $select->offset($options['offset']);
            }
        }

        $select->where->and->equalTo('delete_flag', 'n');

        $select->order(['page_details.page_detail_id ASC']);
        //var_dump($select->getSqlString());
        return $this->tableGateway->selectWith($select);
    }

    public function getPagesCount($pageCategoryCode = 'EVENT_PAGE', $eventCategoryID = 0, $dateRange = [])
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(['pages_count' => new Expression('DISTINCT(page_details.page_detail_id)')]);
        $select->join(['pc' => 'page_contents'], 'page_details.page_detail_id = pc.page_detail_id', [], 'INNER');
        $select->join(['ftc' => 'form_template_contents'], 'pc.form_template_content_id = ftc.form_template_content_id', [], 'INNER');
        $select->where(['page_category_code' => $pageCategoryCode]);

        if (!empty($eventCategoryID)) {
            $select->where->and;
            $select->where->equalTo('ftc.event_category', $eventCategoryID);
        }

        if (!empty($dateRange)) {
            $isStartDateSet = false;
            $isEndDateSet = false;

            if (!empty($dateRange['start_date'])) {
                $select->where->greaterThanOrEqualTo('event_date', $dateRange['start_date']);
                $isStartDateSet = true;
            }

            if (!empty($dateRange['end_date'])) {
                if ($isStartDateSet) {
                    $select->where->and;
                }

                $isEndDateSet = true;
                $select->where->lessThanOrEqualTo('event_date', $dateRange['end_date']);
            }
        }

        $select->where->and->equalTo('delete_flag', 'n');

        $select->group('page_details.page_detail_id');
//        var_dump($select->getSqlString());
        return $this->tableGateway->selectWith($select);
    }

    public function getPageDetailsAndContent($pageDetailID)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'page_detail_id' => new Expression('page_details.page_detail_id'),
            'page_name', 'page_description', 'page_category_code', 'template_code','user_credential_name',
            'json_content' => new Expression('ftc.json_content'),
            'event_date' => new Expression('ftc.event_date'),
            'event_category' => new Expression('ftc.event_category'),
            'event_category_name' => new Expression('ec.event_category_name')
            ]);
        $select->join(['pc' => 'page_contents'], 'page_details.page_detail_id = pc.page_detail_id', [], 'INNER');
        $select->join(['ftc' => 'form_template_contents'], 'pc.form_template_content_id = ftc.form_template_content_id', [], 'INNER');
        $select->join(['ec' => 'event_categories'], 'ftc.event_category = ec.event_category_id', [], 'LEFT');
        $select->where(['page_details.page_detail_id' => $pageDetailID, 'page_details.delete_flag' => 'n']);

        return $this->tableGateway->selectWith($select);
    }
}
