<?php
namespace Page\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class PageContentsTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchPageContentUsingPageDetailID($pageDetailID) {
        $select = $this->tableGateway->getSql()->select();
        $select->columns([
            'json_content' => new Expression('ftc.json_content')
        ]);
        $select->join(['ftc' => 'form_template_contents'], 'page_contents   .form_template_content_id = ftc.form_template_content_id', [], 'INNER');
        $select->where(['page_contents.page_detail_id' => $pageDetailID]);

        return $this->tableGateway->selectWith($select);
    }


    public function insertPageContent($data) {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }
}
