<?php
namespace Page\Model;

class PageDetail {
    public $page_detail_id;
    public $page_name;
    public $page_description;
    public $page_url;
    public $template_code;
    public $page_category_code;
    public $user_credential_id;
    public $user_credential_name;
    public $date_created;

    public function exchangeArray($data) {

    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }
}
