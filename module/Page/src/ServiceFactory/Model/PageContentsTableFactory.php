<?php
namespace Page\ServiceFactory\Model;

use Page\Model\PageContent;
use Page\Model\PageContentsTable;
use Psr\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class PageContentsTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new PageContent());

        $tableGateway = new TableGateway(
            'page_contents',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new PageContentsTable($tableGateway);
    }
}
