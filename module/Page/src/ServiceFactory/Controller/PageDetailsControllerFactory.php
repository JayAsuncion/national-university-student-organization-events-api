<?php
namespace Page\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Page\Controller\PageDetailsController;
use Page\Model\PageContentsTable;
use Page\Model\PageDetailsTable;
use Psr\Container\ContainerInterface;

class PageDetailsControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $pageContentsTable = $container->get(PageContentsTable::class);
        $pageDetailsTable = $container->get(PageDetailsTable::class);

        return new PageDetailsController($authService, $pageContentsTable, $pageDetailsTable);
    }
}
