<?php
namespace Resolution;

use Resolution\Controller\ResolutionListController;
use Resolution\Model\ResolutionTable;
use Resolution\ServiceFactory\Controller\ResolutionListControllerFactory;
use Resolution\ServiceFactory\Model\ResolutionTableFactory;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'resolutions' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/resolutions[/:id]',
                    'defaults' => [
                        'controller' => ResolutionListController::class
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            ResolutionListController::class => ResolutionListControllerFactory::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            ResolutionTable::class => ResolutionTableFactory::class
        ],
        'invokables' => [

        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ]
];
