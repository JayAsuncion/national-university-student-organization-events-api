<?php
namespace Resolution\Model;

use Zend\Db\TableGateway\TableGateway;

class ResolutionTable
{
    private $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getResolutionList()
    {
        $select = $this->tableGateway->getSql()->select();
        $select->where(['delete_flag' => 'n']);
        return $this->tableGateway->selectWith($select);
    }

    public function getResolution($resolutionID)
    {
        return $this->tableGateway->select(['resolution_id' => $resolutionID, 'delete_flag' => 'n']);
    }
}
