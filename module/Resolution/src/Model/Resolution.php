<?php
namespace Resolution\Model;

class Resolution
{
    private $resolution_id;
    private $resolution_title;
    private $resolution_description;
    private $resolution_url;
    private $uploaded_by;
    private $uploaded_at;

    public function exchangeArray($data)
    {
        $this->resolution_id = !empty($data['resolution_id']) ? $data['resolution_id'] : '';
        $this->resolution_title = !empty($data['resolution_title']) ? $data['resolution_title'] : '';
        $this->resolution_description = !empty($data['resolution_description']) ? $data['resolution_description'] : '';
        $this->resolution_url = !empty($data['resolution_url']) ? $data['resolution_url'] : '';
        $this->uploaded_by = !empty($data['uploaded_by']) ? $data['uploaded_by'] : '';
        $this->uploaded_at = !empty($data['uploaded_at']) ? $data['uploaded_at'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
