<?php
namespace Resolution\ServiceFactory\Controller;

use Auth\Service\AuthService;
use Psr\Container\ContainerInterface;
use Resolution\Controller\ResolutionListController;
use Resolution\Model\ResolutionTable;

class ResolutionListControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthService::class);
        $resolutionTable = $container->get(ResolutionTable::class);

        return new ResolutionListController(
            $authService,
            $resolutionTable
        );
    }
}
