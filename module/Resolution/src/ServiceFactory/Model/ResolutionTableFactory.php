<?php
namespace Resolution\ServiceFactory\Model;

use Psr\Container\ContainerInterface;
use Resolution\Model\Resolution;
use Resolution\Model\ResolutionTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class ResolutionTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('nusg');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Resolution());

        $tableGateway = new TableGateway(
            'resolutions',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new ResolutionTable($tableGateway);
    }
}
