<?php
namespace Resolution\Controller;

use Application\Controller\AppAbstractRestfulController;
use Auth\Service\AuthService;
use Resolution\Model\ResolutionTable;
use Zend\View\Model\JsonModel;

class ResolutionListController extends AppAbstractRestfulController
{
    private $resolutionTable;

    public function __construct(
        AuthService $authService,
        ResolutionTable $resolutionTable
    ) {
        parent::__construct($authService);
        $this->resolutionTable = $resolutionTable;
    }

    public function getList()
    {
        $resolutionListResultSet = $this->resolutionTable->getResolutionList();
        $resolutionList = iterator_to_array($resolutionListResultSet->getDataSource());

        return new JsonModel([
            'success' => true,
            'data' => [
                'resolution_list' => $resolutionList
            ]
        ]);
    }

    public function get($id)
    {
        $resolutionResultSet = $this->resolutionTable->getResolution($id);
        $resolution = $resolutionResultSet->getDataSource()->current();

        if (empty($resolution)) {
            return new JsonModel([
                'success' => false,
                'message' => 'Resolution #' . $id . ' not found.'
            ]);
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'resolution' => $resolution
            ]
        ]);
    }
}
