<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

use Zend\Db\Adapter\AdapterAbstractServiceFactory;

return array(
    'service_manager' => array(
        'abstract_factories' => array(
            AdapterAbstractServiceFactory::class,
        ),
    ),
    //    'db' => array(
    //        'adapters' => array(
    //            'nusg' => array(
    //                'driver' => 'Pdo_Mysql',
    //                'driver_options' => array(
    //                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
    //                )
    //            ),
    //        ),
    //    ),
);
